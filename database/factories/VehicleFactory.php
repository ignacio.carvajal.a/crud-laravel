<?php

namespace Database\Factories;

use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Factories\Factory;

class VehicleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Vehicle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'version' => $this->faker->colorName,
            'precio' => $this->faker->numberBetween(1000, 1000000),
            'anio' => $this->faker->year,
            'kilometraje' => $this->faker->numberBetween(1000, 12000),
            'imagen' => '',
            'estado' => true,
            'fecha_ingreso' => now()
        ];
    }
}
