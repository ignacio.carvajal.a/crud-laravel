<?php


namespace App\Repositories;


use App\Models\BrandModel;
use Illuminate\Support\Collection;

class BrandModelRepository
{
    public function getAll() : Collection
    {
        return BrandModel::all();
    }

    public function getAllByBrand(int $brandId) : Collection
    {
        return BrandModel::where('id_marca', $brandId)->get();
    }
}
