<?php


namespace App\Repositories;


use App\Models\Vehicle;
use Illuminate\Pagination\LengthAwarePaginator;

class VehicleRepository
{
    public function getAll() : LengthAwarePaginator
    {
        return Vehicle::paginate(10);
    }

    public function create(array $data) : Vehicle
    {
        return Vehicle::create($data);
    }

    public function findById(int $id) : ?Vehicle
    {
        return Vehicle::findOrFail($id);
    }

    public function update(int $id, array $data) : bool
    {
        return $this->findById($id)->update($data);
    }

    public function delete(int $id) : bool
    {
        return $this->findById($id)->delete();
    }
}
