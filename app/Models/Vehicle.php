<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;

    protected $table = "vehiculos";

    public $timestamps = false;

    protected $fillable = [
        'id_marca',
        'id_modelo',
        'version',
        'precio',
        'anio',
        'kilometraje',
        'imagen',
        'estado',
        'fecha_ingreso',
    ];

    protected $casts = [
        'fecha_ingreso' => 'datetime'
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'id_marca');
    }

    public function model()
    {
        return $this->belongsTo(BrandModel::class, 'id_modelo');
    }
}
