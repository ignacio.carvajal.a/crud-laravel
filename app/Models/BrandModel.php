<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrandModel extends Model
{
    use HasFactory;

    protected $table = "modelos";

    public $timestamps = false;

    public $incrementing = false;

    protected $fillable = [
        'descripcion',
    ];

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'id_marca');
    }
}
