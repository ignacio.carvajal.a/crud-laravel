<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;

    protected $table = "marcas";

    public $timestamps = false;

    protected $fillable = [
        'descripcion',
    ];

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }

    public function models()
    {
        return $this->hasMany(BrandModel::class);
    }
}
