<?php


namespace App\Services;


use App\Exceptions\ErrorDeleteImageVehicleException;
use App\Exceptions\ErrorUploadImageVehicleException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class VehicleService
{
    /**
     * @throws ErrorUploadImageVehicleException
     */
    public function uploadImage($imageTmp) : string
    {
        $newImageName = now()->format('dmYhis').'.'.$imageTmp->extension();
        if (!$imageTmp->move(public_path('uploads/images'), $newImageName)) {
            throw new ErrorUploadImageVehicleException('Error al subir la imagen del vehículo');
        }

        return asset('uploads/images/'. $newImageName);
    }

    /**
     * @param String $image
     * @return void
     * @throws ErrorDeleteImageVehicleException
     */
    public function deleteImage(String $image) : void
    {
        $imgName = Str::afterLast($image, '/');
        if (!Storage::disk('uploads')->delete('uploads/images/'. $imgName)) {
            throw new ErrorDeleteImageVehicleException('Error al eliminar la imagen del vehículo');
        }
    }
}
