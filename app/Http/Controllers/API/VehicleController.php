<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreVehicleRequest;
use App\Http\Requests\UpdateVehicleRequest;
use App\Http\Resources\VehicleCollection;
use App\Http\Resources\VehicleResource;
use App\Models\Vehicle;
use App\Repositories\VehicleRepository;
use App\Services\VehicleService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class VehicleController extends Controller
{
    private $vehicleRepository;
    private $vehicleService;

    public function __construct(VehicleRepository $vehicleRepository, VehicleService $vehicleService)
    {
        $this->vehicleRepository = $vehicleRepository;
        $this->vehicleService = $vehicleService;
    }

    public function index (): VehicleCollection
    {
        return new VehicleCollection($this->vehicleRepository->getAll());
    }

    public function show ($id): VehicleResource
    {
        return new VehicleResource($this->vehicleRepository->findById($id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreVehicleRequest $request
     * @return VehicleResource
     * @throws \App\Exceptions\ErrorUploadImageVehicleException
     */
    public function store(StoreVehicleRequest $request): VehicleResource
    {
        if ($request->hasFile('imagen_tmp')) {
            $data = array_merge($request->validated(), ['imagen' => $this->vehicleService->uploadImage($request->imagen_tmp)]);
        } else {
            $data = $request->validated();
        }

        return new VehicleResource($this->vehicleRepository->create($data));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateVehicleRequest $request
     * @param Vehicle $vehicle
     * @return bool
     * @throws \App\Exceptions\ErrorDeleteImageVehicleException|\App\Exceptions\ErrorUploadImageVehicleException
     */
    public function update(UpdateVehicleRequest $request, Vehicle $vehicle): bool
    {
        if ($vehicle->imagen && $request->hasFile('imagen_tmp')) {
            $this->vehicleService->deleteImage($vehicle->imagen);
        }

        if ($request->hasFile('imagen_tmp')) {
            $data = array_merge($request->validated(), ['imagen' => $this->vehicleService->uploadImage($request->imagen_tmp)]);
        } else {
            $data = $request->validated();
        }

        return $this->vehicleRepository->update($vehicle->id, $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Vehicle $vehicle
     * @return bool
     * @throws \App\Exceptions\ErrorDeleteImageVehicleException
     */
    public function destroy(Vehicle $vehicle) : bool
    {
        if ($vehicle->imagen) {
            $this->vehicleService->deleteImage($vehicle->imagen);
        }

        return $this->vehicleRepository->delete($vehicle->id);
    }
}
