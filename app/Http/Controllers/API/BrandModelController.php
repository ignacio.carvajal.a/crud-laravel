<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\BrandModelCollection;
use App\Repositories\BrandModelRepository;
use Illuminate\Http\Request;

class BrandModelController extends Controller
{
    private $brandModelRepository;

    public function __construct(BrandModelRepository $brandModelRepository)
    {
        $this->brandModelRepository = $brandModelRepository;
    }

    public function index(int $brandId): BrandModelCollection
    {
        return new BrandModelCollection($this->brandModelRepository->getAllByBrand($brandId));
    }
}
