<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class VehicleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'id_marca'      => $this->id_marca,
            'id_modelo'     => $this->id_modelo,
            'marca'         => $this->brand->descripcion,
            'modelo'        => $this->model->descripcion,
            'version'       => $this->version,
            'precio'        => $this->precio,
            'anio'          => $this->anio,
            'kilometraje'   => $this->kilometraje,
            'imagen'        => $this->imagen,
            //'imagen_exists' => @get_headers($this->imagen),
            'estado'        => $this->estado,
            'estado_text'   => $this->estado ? 'Activo' : 'Inactivo',
            'fecha_ingreso' => Carbon::parse($this->fecha_ingreso)->format('d-m-Y H:i'),
        ];
    }
}
