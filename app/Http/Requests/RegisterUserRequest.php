<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => ['required', 'max:55'],
            'email'                 => ['email', 'required', 'unique:users'],
            'password'              => ['required', 'confirmed'],
            'password_confirmation' => ['required', 'same:password']
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre es obligatorio',
            'email.required' => 'El email es obligatorio',
            'email.email' => 'El email es inválido',
            'password.required' => 'La contraseña es obligatoria',
            'password_confirmation.required' => 'Confirme su contraseña',
            'password_confirmation.same' => 'Las contraseñas no coinsiden',
        ];
    }
}
