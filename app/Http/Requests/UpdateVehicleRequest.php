<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateVehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /*$uniqueVehicle = Rule::unique('vehiculos')->where(function ($q) {
            $q->where('id_marca', $this->id_marca)
                ->where('id_modelo', $this->id_modelo)
                ->where('version', $this->version)
                ->where('anio', $this->anio)
                ->where('id', '!=', $this->route()->parameters()['vehicle']);
        });*/

        return [
            'id_marca'    => ['required'],
            'id_modelo'   => ['required'],
            'version'     => ['required'],
            'anio'        => ['required', 'numeric', 'date_format:Y', 'min:1900', 'max:'. now()->format('Y')],
            'precio'      => ['required', 'numeric', 'min:1', 'max: 1000000000'],
            'estado'      => ['required', 'boolean'],
            'kilometraje' => ['nullable', 'numeric'],
            'imagen'      => ['nullable', 'image', 'mimes:jpeg,jpg,png'],
        ];
    }

    public function messages()
    {
        return [
            'id_marca.required'  => 'La marca del vehículo es obligatoria',
            'id_marca.unique'    => 'El vehículo ya esta registrado con esta marca, id_modeloo, versíon y año',
            'id_modelo.required' => 'El id_modeloo del vehículo es obligatoria',
            'version.required'   => 'La versión del vehículo es obligatoria',
            'anio.required'      => 'El año del vehículo es obligatoria',
            'anio.date_format'   => 'Formato de año incorrecto',
            'anio.min'           => 'El año debe ser posterior al 1900',
            'precio.required'    => 'El precio del vehículo es obligatoria',
            'precio.min'         => 'El precio debe ser superior a $0 ',
            'imagen.image'       => 'Ingrese una imagen válida',
            'imagen.mimes'       => 'El formato debe ser jpeg,jpg ó png',
        ];
    }
}
