<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResource('vehicles', \App\Http\Controllers\API\VehicleController::class)->middleware('auth:api');

Route::get('/vehiculos', [\App\Http\Controllers\API\VehicleController::class, 'index']);

Route::post('/register', [\App\Http\Controllers\Auth\RegisterController::class, 'register'])->name('register');
Route::post('/login', [\App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login');

Route::get('/brands', [\App\Http\Controllers\API\BrandController::class, 'index']);
Route::get('/brands/{brand}/models', [\App\Http\Controllers\API\BrandModelController::class, 'index']);
