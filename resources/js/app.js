require('./bootstrap');

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from "./store";

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.getters["user/loggedIn"]) {
            next({
                name: 'login',
            });
        } else {
            next();
        }
    } else {
        next();
    }
});

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
        App
    },
});
