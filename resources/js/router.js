import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import Login from './views/Login';
import Register from "./views/Register";
import Create from "./components/vehicles/Create";
import Vehicles from "./views/Vehicles";
import Edit from "./components/vehicles/Edit";

// Routes
const router = new VueRouter({
    mode: 'history',
    linkActiveClass: 'is-active',
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/vehicles',
            name: 'vehicles',
            component: Vehicles,
            meta : {
                requiresAuth : true
            }
        },
        {
            path: '/vehicles/create',
            name: 'create_vehicle',
            component: Create,
            meta : {
                requiresAuth : true
            }
        },
        {
            path: '/vehicles/:id/edit',
            name: 'edit_vehicle',
            component: Edit,
            props: true,
            meta : {
                requiresAuth : true
            }
        },
        {
            path: '/',
            name: 'home',
            meta : {
                requiresAuth : true
            }
        }
    ],
});

export default router;
