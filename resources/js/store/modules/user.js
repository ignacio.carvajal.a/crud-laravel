
// Actions
import userActions from './actions/userActions';

// Mutators
import userMutations from './mutations/userMutations';

// Mutators
import userGetters from './getters/userGetters';


// initial state
const state = () => ({
    user : null,
    token : null,
    email : '',
    password : '',
    errors : []
});

export default {
    namespaced: true,
    state,
    getters: userGetters,
    actions: userActions ,
    mutations: userMutations
}
