import {
    RESET_VEHICLE,
    UPDATE_BRAND_ID_VEHICLE,
    UPDATE_BRANDS_LIST,
    UPDATE_ERRORS_STORE, UPDATE_ID_VEHICLE,
    UPDATE_IMAGE_TMP_VEHICLE,
    UPDATE_IMAGE_VEHICLE,
    UPDATE_KILOMETERS_VEHICLE,
    UPDATE_MODEL_ID_VEHICLE,
    UPDATE_MODELS_LIST,
    UPDATE_PRICE_VEHICLE,
    UPDATE_STATUS_VEHICLE,
    UPDATE_VEHICLES,
    UPDATE_VERSION_VEHICLE,
    UPDATE_YEAR_VEHICLE
} from "../types/vehicleTypes";
import router from "../../../router";
import dateFormat from 'dateformat';
import Swal from "sweetalert2";

export default {
    findAll: ({ commit , rootState}, nextPage) => {
        const url = nextPage ? nextPage : '/api/vehicles';

        axios.get(url, {
            headers: { Authorization: "Bearer " + rootState.user.token }
        }).then((response) => {
            commit(UPDATE_VEHICLES, response.data);
        }).catch((error) => {
            console.log(error)
        })
    },

    vehicles: () => {
        router.push('/vehicles');
    },

    create: ({ commit }) => {
        commit(RESET_VEHICLE);
        router.push('/vehicles/create');
    },

    findBrands: ({ commit, rootState }) => {
        axios.get('/api/brands', {
            headers: { Authorization: "Bearer " + rootState.user.token }
        }).then((response) => {
            commit(UPDATE_BRANDS_LIST, response.data.data);
        });
    },

    findModelsByBrand: ({ commit, state, rootState }, brandId) => {
        axios.get(`/api/brands/${brandId}/models`, {
            headers: { Authorization: "Bearer " + rootState.user.token }
        }).then((response) => {
            commit(UPDATE_MODELS_LIST, response.data.data);
        });
    },

    findById: ({ commit, rootState, dispatch }, id) => {
        axios.get(`/api/vehicles/${id}`, {
            headers: { Authorization: "Bearer " + rootState.user.token }
        }).then((response) => {
            const vehicle = response.data.data;
            commit(UPDATE_ID_VEHICLE, vehicle.id);
            commit(UPDATE_BRAND_ID_VEHICLE, vehicle.id_marca);
            commit(UPDATE_MODEL_ID_VEHICLE, vehicle.id_modelo);
            commit(UPDATE_VERSION_VEHICLE, vehicle.version);
            commit(UPDATE_YEAR_VEHICLE, vehicle.anio);
            commit(UPDATE_KILOMETERS_VEHICLE, vehicle.kilometraje);
            commit(UPDATE_PRICE_VEHICLE, vehicle.precio);
            commit(UPDATE_STATUS_VEHICLE, vehicle.estado);
            commit(UPDATE_IMAGE_TMP_VEHICLE, vehicle.imagen);

            dispatch('findModelsByBrand', vehicle.id_marca);
        })
    },

    updateBrandId: ({ commit }, brandId) => {
        commit(UPDATE_BRAND_ID_VEHICLE, brandId);
    },

    updateModeldId: ({ commit }, modelId) => {
        commit(UPDATE_MODEL_ID_VEHICLE, modelId);
    },

    updateVersion: ({ commit }, version) => {
        commit(UPDATE_VERSION_VEHICLE, version);
    },

    updateYear: ({ commit }, year) => {
        commit(UPDATE_YEAR_VEHICLE, year);
    },

    updateKilometers: ({ commit }, kilometeres) => {
        commit(UPDATE_KILOMETERS_VEHICLE, kilometeres);
    },

    updatePrice: ({ commit }, price) => {
        commit(UPDATE_PRICE_VEHICLE, price);
    },

    updateStatus: ({ commit }, status) => {
        commit(UPDATE_STATUS_VEHICLE, status);
    },

    createImage: ({ commit, state }, e) => {
        //Campo que se manda al backend
        commit(UPDATE_IMAGE_VEHICLE, e.target.files[0]);

        //Para la vista previa
        let files = e.target.files || e.dataTransfer.files;
        if (!files.length)
            return;

        let reader = new FileReader();
        reader.onload = (e) => {
            commit(UPDATE_IMAGE_TMP_VEHICLE, e.target.result);
        };
        reader.readAsDataURL(files[0]);
    },

    store: ({ commit, state, rootState }) => {
        const formData = new FormData;
        formData.append('id_marca', state.vehicle.brand_id);
        formData.append('id_modelo', state.vehicle.model_id);
        formData.append('version', state.vehicle.version);
        formData.append('precio', state.vehicle.price);
        formData.append('anio', state.vehicle.year);
        formData.append('kilometraje', state.vehicle.kilometers);

        if(state.vehicle.image){
            formData.append('imagen_tmp',state.vehicle.image);
        }

        formData.append('estado', state.vehicle.status);
        formData.append('fecha_ingreso', dateFormat(new Date(), 'isoDateTime'));

        axios.post('/api/vehicles', formData,{
                headers: { Authorization: "Bearer " + rootState.user.token }
            })
            .then((response) => {
                Swal.fire('¡Éxito!', 'Se ha ingresado el nuevo vehículo ', 'success').then(() => {
                    router.push('/vehicles')
                });
            }).catch((error) => {
                if (error.response.status === 422) {
                    commit(UPDATE_ERRORS_STORE, error.response.data.errors);
                } else {
                    Swal.fire('¡Error!', 'Error al ingresar un nuevo vehículo', 'warning');
                }
        });
    },

    edit: ({}, id) => {
        router.push(`/vehicles/${id}/edit`);
    },

    destroy: ({ dispatch, rootState }, id) => {
        Swal.fire({
            title: '¿Estas seguro?',
            text: "El vehículo dejara de aparecer en el listado",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar',
            cancelButtonText : 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                axios.delete(`/api/vehicles/${id}`, {
                    headers: { Authorization: "Bearer " + rootState.user.token }
                }).then((response) => {
                    Swal.fire('¡Éxito!', 'Se ha eliminado el vehículo ', 'success').then(() => {
                        dispatch('findAll');
                    });
                }).catch((error) => {
                    Swal.fire('¡Error!', 'Error al eliminar el vehículo', 'warning');
                });
            }
        });
    },

    update: ({ state, commit, rootState }) => {
        const formData = new FormData;
        formData.append('id_marca', state.vehicle.brand_id);
        formData.append('id_modelo', state.vehicle.model_id);
        formData.append('version', state.vehicle.version);
        formData.append('precio', state.vehicle.price);
        formData.append('anio', state.vehicle.year);
        formData.append('kilometraje', state.vehicle.kilometers);

        if(state.vehicle.image){
            formData.append('imagen_tmp',state.vehicle.image);
        }

        formData.append('estado', state.vehicle.status);
        formData.append('fecha_ingreso', dateFormat(new Date(), 'isoDateTime'));
        formData.append('_method','PUT');

        axios.post(`/api/vehicles/${state.vehicle.id}`, formData, {
            headers: { Authorization: "Bearer " + rootState.user.token }
        }).then((response) => {
            Swal.fire('¡Éxito!', 'Se ha modificado el vehículo ', 'success').then(() => {
                router.push('/vehicles')
            });
        }).catch((error) => {
            if (error.response.status === 422) {
                commit(UPDATE_ERRORS_STORE, error.response.data.errors);
            } else {
                Swal.fire('¡Error!', 'Error al modificar el vehículo', 'warning');
            }
        });
    }
}
