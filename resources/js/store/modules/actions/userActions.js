import {UPDATE_EMAIL, UPDATE_ERRORS, UPDATE_PASSWORD, UPDATE_TOKEN, UPDATE_USER} from '../types/userTypes';
import router from '../../../router';

export default {
    updateEmail: ({ commit }, email) => {
        commit(UPDATE_EMAIL, email);
    },

    updatePassword: ({ commit }, password) => {
        commit(UPDATE_PASSWORD, password);
    },


    login: ({ state, commit }) => {
        commit(UPDATE_ERRORS, []);

        axios.post('/api/login', {
            email : state.email,
            password : state.password
        }).then((response) => {
            const { user, access_token} = response.data;
            commit(UPDATE_USER, user);
            commit(UPDATE_TOKEN, access_token);
            router.push('/vehicles');
        }).catch((error) => {
            let errors = [];
            if (error.response.status === 422) {
                for (let errorKey in error.response.data.errors) {
                    errors.push(error.response.data.errors[errorKey][0]);
                }
            } else {
                errors.push(error.response.data.message);
            }
            commit(UPDATE_ERRORS, errors);
        });
    },

    logout: ({ commit }) => {
        commit(UPDATE_USER, null);
        commit(UPDATE_TOKEN, null);
        router.push('/login');
    }
}
