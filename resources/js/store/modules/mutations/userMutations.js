import {UPDATE_EMAIL, UPDATE_ERRORS, UPDATE_PASSWORD, UPDATE_TOKEN, UPDATE_USER} from '../types/userTypes';

export default {
    [UPDATE_EMAIL] (state, email) {
        state.email = email;
    },

    [UPDATE_PASSWORD] (state, password) {
        state.password = password;
    },

    [UPDATE_ERRORS] (state, errors) {
        state.errors = errors;
    },

    [UPDATE_TOKEN] (state, token) {
        state.token = token;
    },

    [UPDATE_USER] (state, data) {
        state.user = data;
    }
}
