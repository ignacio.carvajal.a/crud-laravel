import {
    RESET_VEHICLE,
    UPDATE_BRAND_ID_VEHICLE,
    UPDATE_BRANDS_LIST,
    UPDATE_ERRORS_STORE,
    UPDATE_ID_VEHICLE,
    UPDATE_IMAGE_TMP_VEHICLE,
    UPDATE_IMAGE_VEHICLE,
    UPDATE_KILOMETERS_VEHICLE,
    UPDATE_MODEL_ID_VEHICLE,
    UPDATE_MODELS_LIST,
    UPDATE_PRICE_VEHICLE,
    UPDATE_STATUS_VEHICLE,
    UPDATE_VEHICLES,
    UPDATE_VERSION_VEHICLE,
    UPDATE_YEAR_VEHICLE
} from "../types/vehicleTypes";

export default {
    [UPDATE_VEHICLES] (state, vehicles) {
        state.vehicles = vehicles;
    },

    [UPDATE_BRANDS_LIST] (state, brands) {
        state.brands = brands;
    },

    [UPDATE_MODELS_LIST] (state, models) {
        state.brandModels = models;
    },

    [UPDATE_BRAND_ID_VEHICLE] (state, brandId) {
        state.vehicle.brand_id = brandId;
    },

    [UPDATE_MODEL_ID_VEHICLE] (state, modelId) {
        state.vehicle.model_id = modelId
    },

    [UPDATE_VERSION_VEHICLE] (state, version) {
        state.vehicle.version = version;
    },

    [UPDATE_YEAR_VEHICLE] (state, year) {
        state.vehicle.year = year;
    },

    [UPDATE_KILOMETERS_VEHICLE] (state, kilometers) {
        state.vehicle.kilometers = kilometers;
    },

    [UPDATE_PRICE_VEHICLE] (state, price) {
        state.vehicle.price = price;
    },

    [UPDATE_STATUS_VEHICLE] (state, status) {
        state.vehicle.status = status;
    },

    [UPDATE_ERRORS_STORE] (state, errors) {
        state.errors = errors;
    },

    [UPDATE_IMAGE_TMP_VEHICLE] (state, img) {
        state.vehicle.image_tmp = img;
    },

    [UPDATE_IMAGE_VEHICLE] (state, img) {
        state.vehicle.image = img;
    },

    [UPDATE_ID_VEHICLE] (state, id) {
        state.vehicle.id = id;
    },

    [RESET_VEHICLE] (state) {
        Object.assign(state.vehicle, {
            id : '',
            brand_id: '',
            model_id : '',
            version: '',
            year: '',
            kilometers: '',
            price: '',
            status : 1,
            image: '',
            image_tmp : null
        });
    }

}
