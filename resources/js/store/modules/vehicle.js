
// Actions
import vehicleActions from './actions/vehicleActions';

// Mutators
import vehicleMutations from './mutations/vehicleMutations';

// Mutators
import vehicleGetters from './getters/vehicleGetters';


// initial state
const state = ({
    vehicles: [],
    brands: [],
    brandModels: [],
    vehicle: {
        id : '',
        brand_id: '',
        model_id : '',
        version: '',
        year: '',
        kilometers: '',
        price: '',
        status : 1,
        image: '',
        image_tmp : null
    },
    errors: {}
});

export default {
    namespaced: true,
    state,
    getters: vehicleGetters,
    actions: vehicleActions ,
    mutations: vehicleMutations
}
