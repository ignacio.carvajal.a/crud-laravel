import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";
import SecureLS from "secure-ls";
const ls = new SecureLS({ isCompression: false });

import user from "./modules/user";
import vehicle from "./modules/vehicle";

Vue.use(Vuex);

const store = new Vuex.Store({
    modules : {
        user,
        vehicle
    },
    plugins: [
        createPersistedState({
            storage: {
                getItem: key => ls.get(key),
                setItem: (key, value) => ls.set(key, value),
                removeItem: key => ls.remove(key)
            },
            paths : ['user']
        })
    ],
})

export default store;
