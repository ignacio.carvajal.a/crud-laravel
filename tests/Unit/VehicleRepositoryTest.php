<?php

namespace Tests\Unit;

use App\Models\Vehicle;
use App\Repositories\VehicleRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class VehicleRepositoryTest extends TestCase
{
    public function test_get_all()
    {
        $vehicles = (new VehicleRepository())->getall();

        $this->assertInstanceOf( LengthAwarePaginator::class , $vehicles);
    }

    public function test_create()
    {
        $vehicle = (new VehicleRepository())->create([
            'id_marca'      => 8,
            'id_modelo'     => 11,
            'version'       => 'V1',
            'precio'        => 1000,
            'anio'          => now()->format('Y'),
            'kilometraje'   => 1000,
            'imagen'        => '',
            'estado'        => 1,
            'fecha_ingreso' => now(),
        ]);

        $this->assertInstanceOf( Vehicle::class , $vehicle);
        $vehicle->delete();
    }

    public function test_find_by_id()
    {
        $vehicle = (new VehicleRepository())->create([
            'id_marca'      => 8,
            'id_modelo'     => 11,
            'version'       => 'V1',
            'precio'        => 1000,
            'anio'          => now()->format('Y'),
            'kilometraje'   => 1000,
            'imagen'        => '',
            'estado'        => 1,
            'fecha_ingreso' => now(),
        ]);

        $result = (new VehicleRepository())->findById($vehicle->id);
        $this->assertInstanceOf( Vehicle::class , $result);
        $vehicle->delete();
    }

    public function test_find_by_id_on_non_existing_id()
    {
        $this->expectException(ModelNotFoundException::class);

        $result = (new VehicleRepository())->findById(10000000);
    }

    public function test_update()
    {
        $vehicle = (new VehicleRepository())->create([
            'id_marca'      => 8,
            'id_modelo'     => 11,
            'version'       => 'V1',
            'precio'        => 1000,
            'anio'          => now()->format('Y'),
            'kilometraje'   => 1000,
            'imagen'        => '',
            'estado'        => 1,
            'fecha_ingreso' => now(),
        ]);

        $result = (new VehicleRepository())->update($vehicle->id, [
            'estado' => 0
        ]);

        $this->assertEquals(true , $result);
        $vehicle->delete();
    }

    public function test_delete()
    {
        $vehicle = (new VehicleRepository())->create([
            'id_marca'      => 8,
            'id_modelo'     => 11,
            'version'       => 'V1',
            'precio'        => 1000,
            'anio'          => now()->format('Y'),
            'kilometraje'   => 1000,
            'imagen'        => '',
            'estado'        => 1,
            'fecha_ingreso' => now(),
        ]);

        $result = (new VehicleRepository())->delete($vehicle->id);

        $this->assertEquals(true , $result);
    }
}
