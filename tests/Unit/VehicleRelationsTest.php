<?php

namespace Tests\Unit;

use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\Vehicle;
use Database\Factories\BrandFactory;
use Tests\TestCase;

class VehicleRelationsTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_vehicle_brand_model()
    {
        $vehicle = Vehicle::factory()
            ->for(Brand::factory())
            ->for(BrandModel::factory()->for(Brand::factory()), 'model')
            ->make();

        $this->assertInstanceOf(Brand::class, $vehicle->brand);
        $this->assertInstanceOf(BrandModel::class, $vehicle->model);
    }
}
