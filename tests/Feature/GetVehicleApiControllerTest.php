<?php

namespace Tests\Feature;

use App\Models\User;
use App\Repositories\VehicleRepository;
use Tests\TestCase;

class GetVehicleApiControllerTest extends TestCase
{
    public function test_get_all_vehicles_without_credentials()
    {
        $response = $this->getJson('/api/vehicles');

        $response->assertStatus(401);
    }

    public function test_get_all_vehicles_with_credentials()
    {

        $user = User::factory()->create();

        $accessToken = $user->createToken('authToken')->accessToken;

        $response = $this->getJson('/api/vehicles', [
            'Authorization' => 'Bearer '. $accessToken,
            'Accept' => 'application/json'
        ]);

        $allVehicles = (new VehicleRepository())->getAll();

        $response->assertStatus(200);
        $response->assertJsonStructure(['data']);
        $response->assertJsonCount($allVehicles->count(), 'data');
    }
}
