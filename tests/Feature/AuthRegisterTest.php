<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthRegisterTest extends TestCase
{
    use WithFaker;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_register_user_with_missing_data()
    {
        $response = $this->postJson('/api/register', []);

        $response->assertStatus(422);
        $response->assertJsonStructure(['message', 'errors']);
        $response->assertJsonCount( 1, 'errors.name');
        $response->assertJsonCount( 1, 'errors.email');
        $response->assertJsonCount( 1, 'errors.password');
        $response->assertJsonCount( 1, 'errors.password_confirmation');
    }

    public function test_register_user_without_email()
    {
        $response = $this->postJson('/api/register', [
            'name' => 'dani',
            'password' => '123456789',
            'password_confirmation' => '123456789',
        ]);

        $response->assertStatus(422);
        $response->assertJsonStructure(['message', 'errors']);
        $response->assertJsonCount( 1, 'errors.email');
    }

    public function test_register_user_with_wrong_confirmation_password()
    {
        $response = $this->postJson('/api/register', [
            'name' => 'dani',
            'email' => 'dani@gmail.com',
            'password' => '123456789',
            'password_confirmation' => '1234567',
        ]);

        $response->assertStatus(422);
        $response->assertJsonStructure(['message', 'errors']);
        $response->assertJsonCount( 1, 'errors.password_confirmation');
    }

    public function test_register_user_with_invalid_email()
    {
        $response = $this->postJson('/api/register', [
            'name' => 'dani',
            'email' => 'dani',
            'password' => '123456789',
            'password_confirmation' => '123456789',
        ]);

        $response->assertStatus(422);
        $response->assertJsonStructure(['message', 'errors']);
        $response->assertJsonCount( 1, 'errors.email');
    }

    public function test_register_user()
    {
        $response = $this->postJson('/api/register', [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->email,
            'password' => '123456789',
            'password_confirmation' => '123456789',
        ]);

        $response->assertStatus(201);
    }
}
