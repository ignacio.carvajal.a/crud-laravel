<?php

namespace Tests\Feature;

use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthLoginTest extends TestCase
{
    use WithFaker;

    public function test_login_missing_data()
    {
        $response = $this->postJson('/api/login', []);

        $response->assertStatus(422);
        $response->assertJsonCount( 1, 'errors.email');
        $response->assertJsonCount( 1, 'errors.password');
    }

    public function test_login_without_email()
    {
        $response = $this->postJson('/api/login', [
            'password' => 'q231231'
        ]);

        $response->assertStatus(422);
        $response->assertJsonCount( 1, 'errors.email');
    }

    public function test_login_with_wrong_email()
    {
        $response = $this->postJson('/api/login', [
            'email' => 'asdas',
            'password' => 'q231231'
        ]);

        $response->assertStatus(422);
        $response->assertJsonCount( 1, 'errors.email');
    }

    public function test_login_without_password()
    {
        $response = $this->postJson('/api/login', [
            'email' => 'asdasd@asd.cl'
        ]);

        $response->assertStatus(422);
        $response->assertJsonCount( 1, 'errors.password');
    }

    public function test_login_with_invalid_credentials()
    {
        $response = $this->postJson('/api/login', [
            'email' => 'asdasd@asd.cl',
            'password' => '123456782'
        ]);

        $response->assertStatus(401);
    }

    public function test_login_correct()
    {
        $user = (new UserRepository())->create([
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->email,
            'password' => bcrypt('123456789')
        ]);

        $response = $this->postJson('/api/login', [
            'email' => $user->email,
            'password' => '123456789'
        ]);

        $response->assertStatus(200);
    }
}
