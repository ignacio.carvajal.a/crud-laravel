<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\User;
use App\Models\Vehicle;
use Tests\TestCase;

class DeleteVehicleApiControllerTest extends TestCase
{
    public function test_delete_vehicle_without_credentials()
    {
        $brand = Brand::factory()->create();
        $brandModel = BrandModel::factory()->state(function () use ($brand) {
            return ['id_marca' => $brand->id];
        })->create();
        $vehicle = Vehicle::factory()->state(function() use($brand, $brandModel) {
            return [
                'id_marca' => $brand->id,
                'id_modelo' => $brandModel->id
            ];
        })->create();

        $response = $this->deleteJson('/api/vehicles/'. $vehicle->id, [], []);

        $response->assertStatus(401);
    }

    public function test_delete_on_non_existing_vehicle()
    {
        $accessToken = User::factory()->create()->createToken('authToken')->accessToken;

        $response = $this->deleteJson('/api/vehicles/10000000', [], [
            'Authorization' => 'Bearer '. $accessToken,
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(404);
    }

    public function test_delete_vehicle()
    {
        $accessToken = User::factory()->create()->createToken('authToken')->accessToken;
        $brand = Brand::factory()->create();
        $brandModel = BrandModel::factory()->state(function () use ($brand) {
            return ['id_marca' => $brand->id];
        })->create();
        $vehicle = Vehicle::factory()->state(function() use($brand, $brandModel) {
            return [
                'id_marca' => $brand->id,
                'id_modelo' => $brandModel->id
            ];
        })->create();

        $response = $this->deleteJson('/api/vehicles/'. $vehicle->id, [], [
            'Authorization' => 'Bearer '. $accessToken,
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(200);
        $this->assertEquals(true, boolval($response));
    }
}
