<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\User;
use App\Models\Vehicle;
use Tests\TestCase;

class StoreVehicleApiControllerTest extends TestCase
{
    public function test_store_vehicle_without_credentials()
    {
        $response = $this->postJson('/api/vehicles', [], []);

        $response->assertStatus(401);
    }

    public function test_store_vehicle_without_requires()
    {
        $accessToken = User::factory()->create()->createToken('authToken')->accessToken;

        $response = $this->postJson('/api/vehicles', [
        ], [
            'Authorization' => 'Bearer '. $accessToken,
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(422);
        $response->assertJsonStructure(['message', 'errors']);
        $response->assertJsonCount( 1, 'errors.id_marca');
        $response->assertJsonCount( 1, 'errors.id_modelo');
        $response->assertJsonCount( 1, 'errors.version');
        $response->assertJsonCount( 1, 'errors.anio');
        $response->assertJsonCount( 1, 'errors.precio');
    }

    public function test_store_vehicle()
    {
        $accessToken = User::factory()->create()->createToken('authToken')->accessToken;
        $brand = Brand::factory()->create();
        $brandModel = BrandModel::factory()->state(function () use ($brand) {
            return ['id_marca' => $brand->id];
        })->create();

        $response = $this->postJson('/api/vehicles', [
            'id_marca' => $brand->id_marca,
            'id_modelo' => $brandModel->id_modelo,
            'version' => 'v1',
            'anio' => 2000,
            'precio' => 30000,
            'estado' => true,
        ], [
            'Authorization' => 'Bearer '. $accessToken,
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(422);
        $response->assertJsonStructure(['message', 'errors']);
        $response->assertJsonCount( 1, 'errors.id_marca');
    }
}
