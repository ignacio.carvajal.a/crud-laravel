<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\User;
use App\Models\Vehicle;
use Tests\TestCase;

class UpdateVehicleApiControllerTest extends TestCase
{
    public function test_update_vehicle_without_credentials()
    {
        $brand = Brand::factory()->create();
        $brandModel = BrandModel::factory()->state(function () use ($brand) {
            return ['id_marca' => $brand->id];
        })->create();
        $vehicle = Vehicle::factory()->state(function() use($brand, $brandModel) {
            return ['id_marca' => $brand->id, 'id_modelo' => $brandModel->id];
        })->create();

        $response = $this->putJson('/api/vehicles/'. $vehicle->id, [], []);

        $response->assertStatus(401);
    }

    public function test_update_vehicle_without_requires()
    {
        $accessToken = User::factory()->create()->createToken('authToken')->accessToken;
        $brand = Brand::factory()->create();
        $brandModel = BrandModel::factory()->state(function () use ($brand) {
            return ['id_marca' => $brand->id];
        })->create();
        $vehicle = Vehicle::factory()->state(function() use($brand, $brandModel) {
            return ['id_marca' => $brand->id, 'id_modelo' => $brandModel->id];
        })->create();

        $response = $this->putJson('/api/vehicles/'. $vehicle->id, [
        ], [
            'Authorization' => 'Bearer '. $accessToken,
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(422);
        $response->assertJsonStructure(['message', 'errors']);
        $response->assertJsonCount( 1, 'errors.id_marca');
        $response->assertJsonCount( 1, 'errors.id_modelo');
        $response->assertJsonCount( 1, 'errors.version');
        $response->assertJsonCount( 1, 'errors.anio');
        $response->assertJsonCount( 1, 'errors.precio');
    }

    public function test_update_vehicle()
    {
        $accessToken = User::factory()->create()->createToken('authToken')->accessToken;
        $brand = Brand::factory()->create();
        $brandModel = BrandModel::factory()->state(function () use ($brand) {
            return ['id_marca' => $brand->id];
        })->create();
        $vehicle = Vehicle::factory()->state(function() use($brand, $brandModel) {
            return [
                'id_marca' => $brand->id,
                'id_modelo' => $brandModel->id
            ];
        })->create();

        $response = $this->putJson('/api/vehicles/'. $vehicle->id, [
            'id_marca' => $vehicle->id_marca,
            'id_modelo' => $vehicle->id_modelo,
            'version' => $vehicle->version,
            'anio' => $vehicle->anio,
            'precio' => 30000,
            'estado' => true,
        ], [
            'Authorization' => 'Bearer '. $accessToken,
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(200);
        $this->assertEquals(true, boolval($response));
    }
}
